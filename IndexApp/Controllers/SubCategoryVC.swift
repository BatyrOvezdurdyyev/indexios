//
//  SubCategoryVC.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 03/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit
import ImageSlideshow

class SubCategoryVC: UIViewController  {

    @IBOutlet weak var subCategorySlider: ImageSlideshow!
    @IBOutlet weak var sliderV: UIView!
    @IBOutlet weak var subCategoryTV: UITableView!
    
     let kingfisherSource = [KingfisherSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080")!, KingfisherSource(urlString: "https://images.unsplash.com/photo-1447746249824-4be4e1b76d66?w=1080")!, KingfisherSource(urlString: "https://images.unsplash.com/photo-1463595373836-6e0b0a8ee322?w=1080")!]
    
    var pageTitle: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subCategoryTV.backgroundColor = Constants.Colors.tableViewBg
        sliderV.backgroundColor = Constants.Colors.tableViewBg
        self.title = pageTitle
        subCategoryTV.tableFooterView = UIView(frame: .zero)
        imagaSliderInit()
    }
    
    
    // MARK: Image slider pod init
    /***************************************************************/
    
    func imagaSliderInit() {
        subCategorySlider.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
        subCategorySlider.contentScaleMode = UIView.ContentMode.scaleAspectFill
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        pageControl.pageIndicatorTintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        subCategorySlider.pageIndicator = pageControl
        subCategorySlider.slideshowInterval = Double(Constants.slideShowInterval)
        
        subCategorySlider.activityIndicator = DefaultActivityIndicator()
        subCategorySlider.currentPageChanged = { page in
//            print("current page:", page)
        }
        subCategorySlider.setImageInputs(kingfisherSource)
    }

}


extension SubCategoryVC: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: Table view methods
    /***************************************************************/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SubCategoryCell.subCategoryCellId, for: indexPath) as! SubCategoryCell
        cell.name.text = "Det"
        cell.qty.text = "23"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "OrganizationSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
            case "OrganizationSegue":
                if let indexPath = self.subCategoryTV.indexPathForSelectedRow {
                    self.subCategoryTV.deselectRow(at: indexPath, animated: true)
                }
            default:
                print("Default")
        }
    }
    
}
