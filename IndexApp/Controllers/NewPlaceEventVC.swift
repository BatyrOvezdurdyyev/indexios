//
//  NewPlaceEventVC.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 25/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit
import LoaderButton

class NewPlaceEventVC: UIViewController {
    @IBOutlet weak var sendBtn: LoaderButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Новое событие/место"
        
        self.view.backgroundColor = Constants.Colors.tableViewBg
        sendBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        sendBtn.layer.borderWidth = 1
        sendBtn.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        sendBtn.layer.cornerRadius = 4
    }
    
    @IBAction func onSend(_ sender: LoaderButton) {
        sendBtn.startAnimate(loaderType: .circleChasse, loaderColor: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), complete: nil)
        
        if #available(iOS 10.0, *) {
            Timer.scheduledTimer(withTimeInterval: 4, repeats: false) { (timer) in
                self.sendBtn.stopAnimate(complete: nil)
            }
        } else {
             self.sendBtn.stopAnimate(complete: nil)
        }
    }


}
