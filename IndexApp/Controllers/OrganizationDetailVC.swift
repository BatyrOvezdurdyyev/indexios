//
//  OrganizationDetailTV.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 11/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit
import ImageSlideshow

class OrganizationDetailVC: UIViewController {
    
    @IBOutlet weak var organizationDetailTV: UITableView!
    @IBOutlet weak var orgnizationDetailSlider: ImageSlideshow!
    @IBOutlet weak var sliderV: UIView!
    @IBOutlet weak var orgName: UILabel!
    @IBOutlet weak var orgCatetory: UILabel!
    
    var orgAddress = "Atamyrat Nyyazov shayoly, 25"
    var complain =  "Nagilelik bildir"
    var itemNames: [String] = ["Tagamlar","Ortaca hasap", "Tomusky terrasa", "Eltip berme"]
    var itemValue: [String] =  ["Milli we Yewropanyn, Milli we Yewropanyn, Italianyn tagamlary", "20-30 manat", "Bar", "Bar"]
    
    var sectionNumbers: [Int] = []
    
    let callBtn = UIButton(type: .custom)
    
    let kingfisherSource = [KingfisherSource(urlString: "https://i.mycdn.me/image?id=815318609001&t=0&plc=WEB&tkn=*IWr2YOyLXdKb1HjEw-XjjpZVGXM")!, KingfisherSource(urlString: "https://i.mycdn.me/image?id=666184540009&t=0&plc=WEB&tkn=*PDxoVbX2J3nOYs2kRyuVi1dBck0")!, KingfisherSource(urlString: "https://media-cdn.tripadvisor.com/media/photo-s/05/7a/a9/32/merdem.jpg")!]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        organizationDetailTV.backgroundColor = Constants.Colors.tableViewBg
        sliderV.backgroundColor = Constants.Colors.tableViewBg
        organizationDetailTV.rowHeight = UITableView.automaticDimension
        organizationDetailTV.tableFooterView = UIView(frame: .zero)
        
        imageSliderInit()
    }
    
    override func viewDidLayoutSubviews() {
        createCircledButton()
        addAnimationInitFunctionBtn()
    }
    
    private func createCircledButton() {
        callBtn.frame = CGRect(x: view.frame.width - 60, y: 148.5, width: 50, height: 50)
        callBtn.layer.cornerRadius = 0.5 * callBtn.bounds.size.width
        callBtn.clipsToBounds = true
        callBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        callBtn.setImage(UIImage(named: "callNew"), for: .normal)
        
        callBtn.layer.shadowColor = UIColor.black.cgColor
        callBtn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        callBtn.layer.masksToBounds = false
        callBtn.layer.shadowOpacity = 0.8
        
        organizationDetailTV.addSubview(callBtn)
    }
    
    private func addAnimationInitFunctionBtn() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.12
        animation.repeatCount = 6
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: callBtn.center.x - 2, y: callBtn.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: callBtn.center.x + 2, y: callBtn.center.y))
        
        callBtn.layer.add(animation, forKey: "position")
        callBtn.addTarget(self, action: #selector(onCall), for: .touchUpInside)
    }
    
    
    @objc func onCall(sender: UIButton!) {
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cencel", style: .cancel) { (action) in
            print("cancel tapped")
        }
        let action = UIAlertAction(title: "+99365122112", style: .default) { (action) in
            print("call tapped")
        }
        cancel.setValue(UIColor.red, forKey: "titleTextColor")
    
        sheet.addAction(action)
        sheet.addAction(cancel)
        present(sheet, animated: true, completion: nil)
    }
    
    
    func imageSliderInit() {
        orgnizationDetailSlider.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
        orgnizationDetailSlider.contentScaleMode = UIView.ContentMode.scaleAspectFill
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        pageControl.pageIndicatorTintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        orgnizationDetailSlider.pageIndicator = pageControl
        orgnizationDetailSlider.slideshowInterval = Double(Constants.slideShowInterval)
        
        orgnizationDetailSlider.activityIndicator = DefaultActivityIndicator()
        orgnizationDetailSlider.currentPageChanged = { page in
//            print("current page:", page)
        }
        orgnizationDetailSlider.setImageInputs(kingfisherSource)
    }

}


extension OrganizationDetailVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section) {
        case 0:
            return 2
        case 1:
            return 3
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch (indexPath.section) {
        case 0:
            if indexPath.row == 0 {
                let cell = Bundle.main.loadNibNamed("AddressCell", owner: self, options: nil)?.first as! AddressCell
                cell.locationName.text = orgAddress
                cell.locationIcon.image = UIImage(named: "location")
                cell.selectionStyle = .none
                tableView.separatorStyle = .singleLine
                
                return cell
            } else {
                let cell = Bundle.main.loadNibNamed("CallAndFavCell", owner: self, options: nil)?.first as! CallAndFavCell
                cell.selectionStyle = .none
                tableView.separatorStyle = .none
                return cell
            }
            
        case 1:
            let cell = Bundle.main.loadNibNamed("OrgCell", owner: self, options: nil)?.first as! OrgCell
            cell.itemName.text = itemNames[indexPath.row]
            cell.itemValue.text = itemValue[indexPath.row]
            cell.selectionStyle = .none
            tableView.separatorStyle = .none
            
            return cell
        default:
            let cell = Bundle.main.loadNibNamed("ComplainCell", owner: self, options: nil)?.first as! ComplainCell
            cell.complainLabel.text = complain
            cell.complainIcon.image = UIImage(named: "info")
            cell.selectionStyle = .none
            tableView.separatorStyle = .none
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 2:
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ComplainVC") as! ComplainVC
            self.present(viewController, animated: true)
        default:
            print("Nope")
        }
    }
    
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String? {
        switch(section) {
        case 0:return ""
        case 1:return " "
        default :return ""
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return 65.0
        }
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 20.0
        }
        return 10.0
    }
    
}
