//
//  InfoVC.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 23/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit

class InfoVC: UIViewController {

    @IBOutlet weak var infoWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        infoWebView.loadHTMLString("<html><body><p>Hello!</p></body></html>", baseURL: nil)
        
        let url = URL(string: "https://www.hackingwithswift.com/example-code/uikit/how-to-load-a-html-string-into-a-wkwebview-or-uiwebview-loadhtmlstring")
        if let unwrappedUrl = url {
            let req = URLRequest(url: unwrappedUrl)
            let session = URLSession.shared
            
            let task = session.dataTask(with: req) {(data, response, error) in
                if error == nil {
                    self.infoWebView.loadRequest(req)
                } else {
                    print("Error")
                }
            }
            task.resume()
        }
        
    }

    @IBAction func closeView(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    
}
