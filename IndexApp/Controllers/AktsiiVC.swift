//
//  AktsiiVC.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 04/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit

class AktsiiVC: UIViewController {

    @IBOutlet weak var aktsiiTV: UITableView!
    @IBOutlet weak var aktsiiV: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        aktsiiTV.backgroundColor = Constants.Colors.tableViewBg
        aktsiiV.backgroundColor = Constants.Colors.tableViewBg
        
        self.title = "Акции"
        aktsiiTV.tableFooterView = UIView(frame: .zero)
    }

}

extension AktsiiVC: UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Table view methods
    /***************************************************************/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView()
        view.backgroundColor = Constants.Colors.tableViewBg
        let label = UILabel()
        var txt = ""
        switch(section) {
            case 0: txt = "Some header 0"
            case 1: txt = "Some header 1"
            case 2: txt = "Some header 2"
            default : txt = ""
        }
        label.text = txt
        label.frame = CGRect(x: 15.0, y: 5.0, width: Double(Float(tableView.frame.size.width - 10.0)), height: 30.0)
        
        view.addSubview(label)
        return view
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed(SalesAndAfishaCell.salesAndAfishaCellId, owner: self, options: nil)?.first as! SalesAndAfishaCell
        
//        cell.populate(with: aksiiItem[indexPath.row])
        cell.itemImage.image = UIImage(named: "aktsiya")
        cell.itemName.text = "Some sale name"
        cell.itemDate.text = "20.10.2018"
        cell.itemAddress.text = "Colin's"
        
        return cell
    }
    
    
}
