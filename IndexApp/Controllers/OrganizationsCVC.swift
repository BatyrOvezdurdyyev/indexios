//
//  OrganizationsVC.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 08/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit
import ImageSlideshow

class OrganizationsCHV: UICollectionReusableView {
    @IBOutlet weak var organizationsSlider: ImageSlideshow!
}

class OrganizationsCVC: UIViewController {

    @IBOutlet weak var organizationCV: UICollectionView!
    
    var pageMode: String = "organizationList"
    
    var organizations: [Organization] = Organization.getOrganizations();
    let kingfisherSource = [KingfisherSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080")!, KingfisherSource(urlString: "https://images.unsplash.com/photo-1447746249824-4be4e1b76d66?w=1080")!, KingfisherSource(urlString: "https://images.unsplash.com/photo-1463595373836-6e0b0a8ee322?w=1080")!]

    override func viewDidLoad() {
        super.viewDidLoad()
        organizationCV.backgroundColor = Constants.Colors.tableViewBg
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.title = "Some title"
    }
    
    override func viewWillLayoutSubviews() {
        if pageMode == "userFavorites" {
            let layout = self.organizationCV.collectionViewLayout as! UICollectionViewFlowLayout
            layout.sectionInset = UIEdgeInsets(top: -168.0, left: 0.0, bottom: 0.0, right: 0.0)
        }
    }
    
    @IBAction func onCall(_ sender: UIButton) {
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cencel", style: .cancel) { (action) in
            print("cancel tapped")
        }
        let action = UIAlertAction(title: "+99365122112", style: .default) { (action) in
            print("call tapped")
        }
        
        cancel.setValue(UIColor.red, forKey: "titleTextColor")
        
        sheet.addAction(action)
        sheet.addAction(cancel)
        
        present(sheet, animated: true, completion: nil)
    }
    
}


extension OrganizationsCVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK: Collection views methods
    /***************************************************************/
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return organizations.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OrganizationCell.organizationCellId,
                                                      for: indexPath) as! OrganizationCell
        
        cell.populate(with: organizations[indexPath.row])
        return cell
    }
    
    // Set the indexPath of the selected item as the sender for the segue
    private func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath) {
        performSegue(withIdentifier: "OrganizationDetailSegue", sender: self)
    }
    
    func prepareForSegue(segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OrganizationDetailSegue" {
            print("true")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width - 16, height: 235);
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: "OrganizationCollectionVI",
                                                                             for: indexPath) as! OrganizationsCHV

                headerView.organizationsSlider.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
                headerView.organizationsSlider.contentScaleMode = UIView.ContentMode.scaleAspectFill

                let pageControl = UIPageControl()
                pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
                pageControl.pageIndicatorTintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                headerView.organizationsSlider.pageIndicator = pageControl
                headerView.organizationsSlider.slideshowInterval = Double(Constants.slideShowInterval)

                headerView.organizationsSlider.activityIndicator = DefaultActivityIndicator()
                headerView.organizationsSlider.currentPageChanged = { page in
    //                print("current page:", page)
                }

                headerView.organizationsSlider.setImageInputs(kingfisherSource)
            
                if pageMode == "userFavorites" {
                    headerView.isHidden = true
                }
            
            return headerView
        default:
            assert(false, "Unexpected element kind")
        }
    }
    
}

