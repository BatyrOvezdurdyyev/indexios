//
//  ViewController.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 01/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit
import ImageSlideshow
import Alamofire
import SwiftyJSON
import CoreLocation

class MainVC: UIViewController {

    let locationManager = CLLocationManager()
    @IBOutlet weak var slideshow: ImageSlideshow!
 
    let kingfisherSource = [KingfisherSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080")!, KingfisherSource(urlString: "https://images.unsplash.com/photo-1447746249824-4be4e1b76d66?w=1080")!, KingfisherSource(urlString: "https://images.unsplash.com/photo-1463595373836-6e0b0a8ee322?w=1080")!]
    
    @IBOutlet weak var aktsiiBox: UIView!
    @IBOutlet weak var afishaBox: UIView!
    @IBOutlet weak var categoryTable: UITableView!
    @IBOutlet weak var footerBtnVIew: UIView!
    @IBOutlet weak var footerBtn: UIButton!
    
    var categories: [Category] = Category.getCategories();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryTable.backgroundColor = Constants.Colors.tableViewBg

        aktsiiBox.layer.cornerRadius = 4
        aktsiiBox.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        afishaBox.layer.cornerRadius = 4
        afishaBox.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        
        footerBtnVIew.backgroundColor = Constants.Colors.tableViewBg
        footerBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        footerBtn.layer.borderWidth = 1
        footerBtn.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        footerBtn.layer.cornerRadius = 4
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
 
        initImageSlideShow()
        dispatchUsers()
        
        getRestaurants()
    }
    
    @IBAction func onAddEventPlace(_ sender: Any) {
    }
    
    // MARK: Image slider pod init
    /***************************************************************/
    
    func initImageSlideShow () {
        slideshow.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
        slideshow.contentScaleMode = UIView.ContentMode.scaleAspectFill
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        pageControl.pageIndicatorTintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        slideshow.pageIndicator = pageControl
        slideshow.slideshowInterval = Double(Constants.slideShowInterval)
        
        slideshow.activityIndicator = DefaultActivityIndicator()
        slideshow.currentPageChanged = { page in
//            print("current page:", page)
        }
        slideshow.setImageInputs(kingfisherSource)
    }
    
    
    // MARK: Aktsii view click function
    /***************************************************************/
    @IBAction func tapRecongnizerAktsii(_ sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AktsiiVC") as! AktsiiVC
            self.show(viewController, sender: self)
        }
    }
    
    // MARK: Afisha view click function
    @IBAction func tapRecongnizerAfisha(_ sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AfishaVC") as! AfishaVC
            self.show(viewController, sender: self)
        }
    }
    
    
    func getRestaurants() {
//        Loader().show(uiView: view)
        guard let url = URL(string: "http://hyzmatda.com/restaurants") else {
            return
        }
        Alamofire.request(url, method: .get).responseJSON(completionHandler: { (response) in
            guard response.result.isSuccess else {
                print("Ошибка при запросе данных \(String(describing: response.result.error))")
                return
            }
//            self.view.subviews[self.view.subviews.count-1].removeFromSuperview()
            let res = response.data
            do {
                let result = try JSONDecoder().decode(RestaurantsResponse.self, from: res!)
                for res in result.restaurants {
                    print(res.name)
                }
            } catch {
                print("we have an errror \(String(describing: response.error))")
            }
        })
    }
    
    
    func dispatchUsers() {
         guard let url = URL(string: "http://driver.api.yacurier.ru/v1/city-list") else {
            return
        }
      
        Alamofire.request(url, method: .get).responseJSON(completionHandler: { (response) in
            guard response.result.isSuccess else {
                print("Ошибка при запросе данных \(String(describing: response.result.error))")
                return
            }
        
            let res = response.data
            do {
                let result = try JSONDecoder().decode(CityResponseObject.self, from: res!)
                let cities = result.data.items
                print(cities)
            } catch {
                print("we have an errror")
            }
        })
    }
    
}


extension MainVC: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: Table view methods
    /***************************************************************/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if let apps =  apps {
//            return apps.count
//        }
//
//        return 0
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CategoryCell.categoryCellId, for: indexPath) as! CategoryCell
        cell.populate(with: categories[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.getRestaurants()
        if indexPath.row == 0 {
             performSegue(withIdentifier: "FavoritesSegue", sender: self)
        } else {
             performSegue(withIdentifier: "SubCategorySegue", sender: self)
        }
        
       
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     if let indexPath = self.categoryTable.indexPathForSelectedRow {
        switch segue.identifier {
            case "FavoritesSegue":
                let dvc = segue.destination as! OrganizationsCVC
                dvc.pageMode = "userFavorites"
            case "SubCategorySegue":
                let dvc = segue.destination as! SubCategoryVC
                dvc.pageTitle = categories[indexPath.row].name
            default:
                print("Default")
        }
        self.categoryTable.deselectRow(at: indexPath, animated: true)
     }
    }
    
}

extension MainVC: CLLocationManagerDelegate {
    
    //MARK: - Location Manager Delegate Methods
    /***************************************************************/
    
    
    //Write the didUpdateLocations method here:
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count - 1]
        if location.horizontalAccuracy > 0 {
            locationManager.stopUpdatingLocation()
            locationManager.delegate = nil
            
            let lat = String(location.coordinate.latitude)
            let lng = String(location.coordinate.longitude)
            
            print("lat: \(lat), lng: \(lng) ")
            
        }
    }
    
    
    //Write the didFailWithError method here:
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Unavaliable \(error)")
    }
    
    
}

