//
//  ComplainVC.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 15/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit

class ComplainVC: UIViewController {

    @IBOutlet weak var complainTV: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        complainTV.backgroundColor = Constants.Colors.tableViewBg
    }
 
    @IBAction func onCancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true) {
        
        }
    }
}

extension ComplainVC: UITabBarDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ComplainCell", for: indexPath) 
        cell.textLabel?.text = "Место закрылось"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }
    
    
}
