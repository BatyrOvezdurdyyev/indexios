//
//  SalesVC.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 03/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit
class AfishaVC: UIViewController {
    @IBOutlet weak var afishaTV: UITableView!
    @IBOutlet weak var imageV: UIView!
    
    let names = ["a", "b", "c", "d", "e", "a", "b", "c", "d", "e", "a", "b", "c", "d", "e"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        afishaTV.backgroundColor = Constants.Colors.tableViewBg
        imageV.backgroundColor = Constants.Colors.tableViewBg
        
        self.title = "Aфиша"
        afishaTV.tableFooterView = UIView(frame: .zero)
    }
    
}


extension AfishaVC: UITableViewDataSource, UITableViewDelegate {
    
    //MARK: Table view methods
    /***************************************************************/
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView()
        view.backgroundColor = Constants.Colors.tableViewBg
        let label = UILabel()
        var txt = ""
        switch(section) {
        case 0: txt = "20. Sentyabr, Penshenbe"
        case 1: txt = "25. Sentyabr, Penshenbe"
        case 2: txt = "27. Sentyabr, Penshenbe"
        default : txt = ""
        }
        label.text = txt
        label.frame = CGRect(x: 15.0, y: 5.0, width: Double(Float(tableView.frame.size.width - 10.0)), height: 30.0)
        
        view.addSubview(label)
        return view
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("SalesAndAfishaCell", owner: self, options: nil)?.first as! SalesAndAfishaCell
        
        cell.itemImage.image = UIImage(named: "aktsiya")
        cell.itemName.text = "Some sale name"
        cell.itemDate.text = "20.10.2018"
        cell.itemAddress.text = "Colin's"
        
        return cell
    }
    
   
    
}
