//
//  SalesAndAfishaCell.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 16/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit

class SalesAndAfishaCell: UITableViewCell {

    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemDate: UILabel!
    @IBOutlet weak var itemAddress: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
//    super.prepareForReuse()
//        itemImage.image = nil
//        itemName.text?.removeAll()
//        itemDate.text?.removeAll()
//        itemAddress.text?.removeAll()
//    }

//    func populate(with aksiiItem: Aksiya) {
//        itemImage.image = category.name
//        subName.text = category.subName
//        icon.image = category.icon
//    }

    @IBAction func onCall(_ sender: UIButton) {
    }
}

extension UITableViewCell {
    
    static var salesAndAfishaCellId: String {
        return String(describing: self)
    }
    
}
