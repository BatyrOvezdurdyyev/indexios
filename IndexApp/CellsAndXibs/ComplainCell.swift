//
//  ComplainCell.swift
//  TableViewMultipleSection
//
//  Created by Aydogdy Shahyrov on 14/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit

class ComplainCell: UITableViewCell {

    @IBOutlet weak var complainLabel: UILabel!
    @IBOutlet weak var complainIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
