//
//  AddressCell.swift
//  TableViewMultipleSection
//
//  Created by Aydogdy Shahyrov on 13/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit

class AddressCell: UITableViewCell {

    @IBOutlet weak var locationIcon: UIImageView!
    @IBOutlet weak var locationName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
