//
//  OrganizationCell.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 08/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit

class OrganizationCell: UICollectionViewCell {

    @IBOutlet weak var orgImage: UIImageView!
    @IBOutlet weak var orgCategory: UILabel!
    @IBOutlet weak var orgName: UILabel!
    @IBOutlet weak var orgAddress: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    
        orgImage.image = nil
        orgCategory.text?.removeAll()
        orgName.text?.removeAll()
        orgAddress.text?.removeAll()
        distanceLabel.text?.removeAll()
    }
    
    
    func populate(with org: Organization) {
        orgImage.image = org.orgImage
        orgCategory.text = org.orgCategory
        orgName.text = org.orgName
        orgAddress.text = org.orgAddress
        distanceLabel.text = org.distanceLabel
    }
    

    @IBAction func onCall(_ sender: UIButton) {
    }
    
}

extension UICollectionViewCell {
    static var organizationCellId: String {
        return String(describing: self)
    }
}
