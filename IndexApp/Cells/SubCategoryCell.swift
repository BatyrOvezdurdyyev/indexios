//
//  SubCategoryCell.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 03/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit

class SubCategoryCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var qty: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        
        name.text?.removeAll()
        qty.text?.removeAll()
    }
    
//    func populate(with subCategory: SubCategory) {
//        name.text = subCategory.name
//        qty.text = subCategory.qty
//    }

}

extension UITableViewCell {
    
    static var subCategoryCellId: String {
        return String(describing: self)
    }
    
}
