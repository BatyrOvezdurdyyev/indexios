//
//  LocationCell.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 13/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit

class LocationCell: UITableViewCell {
    
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var locationIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

extension UITableViewCell {
    static var LocationCellId: String {
        return String(describing: self)
    }
}
