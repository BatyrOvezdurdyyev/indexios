//
//  CategoryCell.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 01/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit
//import Kingfisher

class CategoryCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var subName: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        name.text?.removeAll()
        subName.text?.removeAll()
        icon.image = nil
    }
    
    
    func populate(with category: Category) {
        name.text = category.name
        subName.text = category.subName
        icon.image = category.icon
//        if let url = URL(string: "http://www.apple.com/euro/ios/ios8/a/generic/images/og.png") {
//            icon.kf.setImage(with: url)
//        }
    }
    
}

extension UITableViewCell {
    
    static var categoryCellId: String {
        return String(describing: self)
    }
    
}

