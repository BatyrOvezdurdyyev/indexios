//
//  GlobalVariables.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 17/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit

struct Constants {
    struct API {
        static let BaseURL = "http//:hyzmatda.com"
    }

    struct Colors {
        static let tableViewBg = UIColorFromHex(rgbValue: 0xEBEBF2, alpha: 1)
    }
    
    static let slideShowInterval = 5
    
}

func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
    let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
    let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
    let blue = CGFloat(rgbValue & 0xFF)/256.0
    
    return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
}
