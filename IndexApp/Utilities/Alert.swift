//
//  Alert.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 17/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit

struct Alert {
    static func errorAlert(title: String, message: String?, cancelButton: Bool = false, completion: (() -> Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "OK", style: .default) {
            _ in
            guard let completion = completion else { return }
            completion()
        }
        alert.addAction(actionOK)
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel)
        if cancelButton { alert.addAction(cancel) }
        
        return alert
    }
    
}

// Creating alerts:
//        let simpleAlert = Alert.errorAlert(title: "Error", message: "Simple message")
//        let alertWithCompletionAndCancel = Alert.errorAlert(title: "Message", message: "Message", cancelButton: true) {
//            print("Btn clicked")
//        }

// Presenting alerts:
//        present(simpleAlert, animated: true)
//        present(alertWithCompletionAndCancel, animated: true) { () in
//            print("You've pressed cancel");
//        }
