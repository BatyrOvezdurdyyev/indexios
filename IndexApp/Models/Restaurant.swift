//
//  Restaurant.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 17/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import Foundation

struct Restaurant: Decodable {
    let website: String
    let desc: String
    let name: String
    let email: [String]
    let long: String
    let lat: String
    let address: String
    let workingTime: String
    let imgPath: String
    let attr1: String
    let phone: [String]
}

struct RestaurantsResponse: Decodable {
    let restaurants: [Restaurant]

    enum CodingKeys: String, CodingKey {
        case restaurants
    }
}
