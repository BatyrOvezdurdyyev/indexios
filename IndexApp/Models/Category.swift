//
//  Categories.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 01/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import Foundation
import UIKit

class Category {
    var id : Int
    var name : String
    var subName : String
    var icon : UIImage
   
    init(id: Int, name : String, subName : String, icon : String){
        
        self.id = id;
        self.name = name;
        self.subName = subName;
        if let image = UIImage(named: icon){
            self.icon = image
        } else {
            self.icon = UIImage(named: "def.png")!
        }
    }
    
    class func getCategories() -> [Category] {
        var cats = [Category]()
        
        cats.append(Category(id: 2, name: "Избранное", subName: "2 места", icon: "def.png"))
        cats.append(Category(id: 0, name: "Еда", subName: "Кафе, рестораны, пицерии, суши-бары", icon: "def.png"))
        cats.append(Category(id: 3, name: "Обучение", subName: "Курсы иностранных языковб автошколы, тренинги", icon: "def.png"))
        cats.append(Category(id: 4, name: "Здаровье и красота", subName: "Салоны красоты, медицинские центры", icon: "def.png"))
        cats.append(Category(id: 5, name: "Детям", subName: "Центры развития, кружки, секции, курсы", icon: "def.png"))
        cats.append(Category(id: 6, name: "Достопримечательности", subName: "Замки, музеи, крепомти и соборы, парки и зоопарки", icon: "def.png"))
        cats.append(Category(id: 7, name: "Активный отдых", subName: "Картинг, пейнтбол, страйкбол, тир, стрельба", icon: "def.png"))
        cats.append(Category(id: 8, name: "Бытовые услуги", subName: "Ремонт техники, одежды и обуви", icon: "def.png"))
        cats.append(Category(id: 8, name: "Авто", subName: "Автосолоны, автосервисы, автомойки", icon: "def.png"))
        cats.append(Category(id: 9, name: "Все для питомцев", subName: "Салоны красоты, зоомагазаны, ветклиники", icon: "def.png"))
        
        return cats;
    }
        
}

//struct Categoty: Decodable {
//    let id: Int
//    let name: String
//    let subName: String
//    let icon: String
//    let subCategories: [SubCategory]
//}
//
//struct SubCategory: Decodable {
//    let id: Int;
//    let name: String;
//    let organizationCount: Int;
//}
//
//struct CategoryResponse: Decodable {
//    let data: [Categoty]
//    let code: Int
//    let limit: Int
//    let offset: Int
//
//    enum CodingKeys: String, CodingKey {
//        case data, code, limit, offset
//    }
//}


