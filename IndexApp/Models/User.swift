//
//  User.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 16/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import Foundation

struct City: Decodable {
    let id: Int
    let city: String
}

struct ResponseCities: Decodable {
    let items: [City]
}

struct CityResponseObject: Decodable {
    let data: ResponseCities
    let offset: Int
    let limit: Int
    let code: Int
    let updatedAt: Date
    
    enum CodingKeys: String, CodingKey {
        case data, offset, limit, code
        case updatedAt = "updated_at"
    }
}
