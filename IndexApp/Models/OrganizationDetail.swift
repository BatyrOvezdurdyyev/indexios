//
//  OrganizationDetail.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 12/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit

class OrganizationDetail {
//    var id: Int;
//    var name: String;
//    var categoryName: String;
//    var images: Array<String>;
//    var address: String;
//    var phones: Array<Phone>;
//    var distance: String;
//    var tariff: Int; // 0, 1, 2
//    var attributes: Array<Attribute>;
//    var complains: Array<Complain>;

//    init(id: Int, name: String, categoryName: String, images: Array<String>, address: String, phones: Array<Phone>, distance: String, tariff: Int, attributes: Array<Attribute>, complains: Array<Complain>){
//
//        self.id = id
//        self.name = name
//        self.categoryName = categoryName
//        self.address = address
//        self.phones = phones
//        self.distance = distance
//        self.tariff = tariff
//        self.attributes = attributes
//        self.complains = complains
//    }
//
//    class func getOrganizationDetail() -> OrganizationDetail {
//
//        var orgDetail = OrganizationDetail(id: 12, name: "Merdem", categoryName: "Medeni dync alysh merkezi", images: [""], address: "Atamyrat Nyyazov shayoly, 25", phones: [], distance: "300 m.", tariff: 1, attributes: [Attribute()], complains: <#T##Array<Complain>#>)
//        
//        return orgDetail
//    }
    
}

class Attribute {
    var name: String = "";
    var detail: String = "";
}

class Complain {
    var id: Int = 0;
    var name: String = "";
}

class Phone {
    var phoneNumber: String = "";
    var name: String = "";
}
