//
//  Organization.swift
//  IndexApp
//
//  Created by Aydogdy Shahyrov on 09/10/2018.
//  Copyright © 2018 Aydogdy Shahyrov. All rights reserved.
//

import UIKit

class Organization {
    var orgImage : UIImage
    var orgCategory : String
    var orgName : String
    var orgAddress : String
    var distanceLabel : String

    init(orgImage: String, orgCategory : String, orgName : String, orgAddress : String, distanceLabel: String){

        if let image = UIImage(named: orgImage){
            self.orgImage = image
        } else {
            self.orgImage = UIImage(named: "def.png")!
        }
        self.orgCategory = orgCategory
        self.orgName = orgName
        self.orgAddress = orgAddress
        self.distanceLabel = distanceLabel
    }

    class func getOrganizations() -> [Organization] {
        var orgs = [Organization]()

        orgs.append(Organization(orgImage: "krazvl.png", orgCategory: "Культурно-развлекательный цент", orgName: "Merdem", orgAddress: "Atamyrat shayoly, 25", distanceLabel: "80 m"))
        orgs.append(Organization(orgImage: "kr.jpeg", orgCategory: "Развлекательный цент", orgName: "Sumbar", orgAddress: "Garashsyzlyk shayoly, 125", distanceLabel: "140 m"))
        orgs.append(Organization(orgImage: "s.png", orgCategory: "Развлекательный цент", orgName: "Sumbar", orgAddress: "Garashsyzlyk shayoly, 125", distanceLabel: "140 m"))
        orgs.append(Organization(orgImage: "razvl.png", orgCategory: "Развлекательный цент", orgName: "Sumbar", orgAddress: "Garashsyzlyk shayoly, 125", distanceLabel: "140 m"))

        return orgs;
    }

}


